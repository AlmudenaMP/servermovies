const mongoose = require('mongoose');

const movieSchema = new mongoose.Schema(
    {
        title: { type: String, required: true, unique: true },
        director: { type: String, required: true },
        year: { type: Number, min: [1888, "En ese año aún no se había hecho ninguna película"] },
        genre: { type: [String], enum: ["Acción", "Animación", "Ciencia ficción", "Comedia romántica"] },
        picture: String
    },
    {
        timestamps: true
    }
);

const Movie = mongoose.model('Movie', movieSchema);

module.exports = Movie;