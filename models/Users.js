//Nuevo modelo para guardar usuarios en la BD

const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        match: [/^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/, 'Por favor, inserte un correo electrónico válido']
    },
    password: { type: String, required: true }
}, {
    timestamps: true
});

const User = mongoose.model('User', userSchema);

module.exports = User;