//Creamos una función de error que nos permite asignar un estado y un mensajae de error

const createError = (msg, status) => {
    const error = new Error(msg);
    error.status = status;
    return error;
};

module.exports = createError;