const passport = require('passport');
const User = require('../../models/Users');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const createError = require('../errors/create-error');

//Método para la estrategia de autenticación de usuarios.
passport.use(
    'register',
    new LocalStrategy({
        usernameField: "email",
        passwordField: "password",
        passReqToCallback: true
    }, async (req, email, password, done) => {
        try {
            const previousUser = await User.findOne({ email });

            if (previousUser) {
                return done(createError('Este usuario ya existe, puedes iniciar sesión'));
            }

            const encPassword = await bcrypt.hash(password, 10);
            const newUser = new User({
                email,
                password: encPassword
            });

            const savedUser = await newUser.save();

            return done(null, savedUser);
        } catch (err) {
            return done(err);
        }
        
    })
);

//Método para la estrategia de LOGIN. Se comprueba primero si el usuario ya existe y después si la contraseña coincide.
passport.use(
    'login',
    new LocalStrategy(
        {
            usernameField: "email",
            passwordField: "password",
            passReqToCallback: true
        },
        async (req, email, password, done) => {
            try {
                const currentUser = await User.findOne({ email });

                if(!currentUser) {
                    return done(createError('No existe un usuario con este email, regístrate'));
                }

                const isValidPassword = await bcrypt.compare(
                    password,
                    currentUser.password
                );

                if (!isValidPassword) {
                    return done(createError('La contraseña es incorrecta'));
                }

                currentUser.password = null;
                return done(null, currentUser);
            } catch (err) {
                return done(err);
            } 
        }
    )
);

//Función para registrar al usuario por us id de la BD
passport.serializeUser((user, done) => {
    return done(null, user._id);
});

//Función para buscar un usuario según su id en la BD
passport.deserializeUser(async (userId, done) => {
    try {
        const existingUser = await User.findById(userId);
        return done(null, existingUser);
    } catch (err) {
        return done(err);
    }
});