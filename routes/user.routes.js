//Guardamos endpoints de registro y similares.

const express = require('express');
const passport = require('passport');

const userRouter = express.Router();

userRouter.post('/register', (req, res, next) => {
    const done = (err, user) =>{
        if (err) {
            return next(err);
        }
        req.logIn(
            user,
            (err) => {
                if (err) {
                    return next(err);
                }
                return res.status(201).json(user);
            }
        );
    };

    //Creación de un autenticador de usuarios y ejecución con la request.
    const authenticator = passport.authenticate('register', done);
    authenticator(req);
});

userRouter.post('/login', (req, res, next) => {
    const done = (err, user) =>{
        if (err) {
            return next(err);
        }
        req.logIn(
            user,
            (err) => {
                if (err) {
                    return next(err);
                }
                return res.status(200).json(user);
            }
        );
    };

    //Creación de un autenticador de logeo y ejecución con la request.
    passport.authenticate('login', done)(req);
});

//Método para que el usuario cierre sesión y se destruya.
userRouter.post('/logout', (req, res, next) => {
    if (req.user) {
        req.logOut(() => {
            req.session.destroy(() => {
                res.clearCookie('connect.sid');
                return res.status(200).json('La sesión se ha cerrado')
        });  
    });
    } else {
        return res.status(304).json('Usuario no ha iniciado sesión');
    }
});




module.exports = userRouter;