const express = require('express');
const Movie = require('../models/Movie.js');
const createError = require('../utils/errors/create-error.js');
const upload = require('../utils/middleware/file.middleware.js');
const imageToUri = require('image-to-uri');
const fs = require('fs');
const uploadToCloudinary = require('../utils/middleware/cloudinary.middleware.js');

const moviesRouter = express.Router();

//Endpoint para recuperar todos los elementos de la colección
moviesRouter.get('/', async (req, res) => {
    try {
        const movies = await Movie.find();
        return res.status(200).json(movies);
    } catch (err) {
        next(err);
    } 
});

//Endpoint para recuperar las películas por su id
moviesRouter.get('/:id', async (req, res, next) => {
    const id = req.params.id;
    try {
        const movies = await Movie.findById(id);
        if (movies) {
            return res.status(200).json(movies);
        } else {
            next(createError('No existe el personaje con id indicado', 404));
        }
    } catch (err) {
        next (err);
    } 
});

//Enpoint para recuperar las películas por su título
moviesRouter.get('/title/:title', async (req, res, next) => {
    const title = req.params.title;
    try {
        const movies = await Movie.find({
            titles: { $in: [title] }
        });
        return res.status(200).json(movies);
    } catch (err) {
        next(err);
    } 
});

//Enpoint para recuperar las películas por su género
moviesRouter.get('/genre/:genre', async (req, res) => {
    const genre = req.params.genre;
    try {
        const moviesG = await Movie.find({
            genre: { $in: [genre] }
        });
        return res.status(200).json(moviesG);
    } catch (err) {
        next(err);
    } 
});

//Enpoint para recuperar las películas estrenadas a partir de 2010
moviesRouter.get('/year/:2010', async (req, res) => {
    const year = req.params.year;
    try {
        const movies = await Movie.find({
            year: { $gte: 2010 }
        });
        return res.status(200).json(movies);
    } catch (err) {
        next(err);
    } 
});

//Método para insertar nuevos elementos en la colección
moviesRouter.post('/', [upload.single('picture')], async (req, res, next) => {
    try {
        const picture = req.file ? req.file.filename : null;
        //Construye un nuevo documento en la colección y lo guarda en ella
        const newMovie = new Movie({ ...req.body, picture });
        const createdMovie = await newMovie.save();
        return res.status(201).json(createdMovie);
    } catch (err) {
        next(err);
    }
});

//Método para insertar nuevos elementos en la colección. Añadirá imágenes con la colección uri.
moviesRouter.post('/with-uri', [upload.single('picture')], async (req, res, next) => {
    try {
        //Transformamos la URL a Base64 de el path que se indica. Una vez transformado se guarda en la BD y luego se borra.
        const filePath = req.file ? req.file.path: null;
        const picture = imageToUri(filePath);
        const newMovie = new Movie({ ...req.body, picture });
        const createdMovie = await newMovie.save();
        await fs.unlinkSync(filePath);
        return res.status(201).json(createdMovie);
    } catch (err) {
        next(err);
    }
});

moviesRouter.post('/to-cloud', [upload.single('picture'), uploadToCloudinary], async (req, res, next) => {
    try {
        const newMovie = new Movie({ ...req.body, picture: req.file_url });
        const createdMovie = await newMovie.save();
        return res.status(201).json(createdMovie);
    } catch (err) {
        next(err);
    }
});

//Método para eliminar por el id elementos de la colección
moviesRouter.delete('/:id', async (req, res, next) => {
    try {
        const id = req.params.id;
        await Movie.findByIdAndDelete(id);
        return res.status(200).json('La película ha sido eliminada correctamente');
    } catch (err) {
        next(err);
    }
});

//Método para modificar elementos de la colección
moviesRouter.put('/:id', async (req, res, next) => {
    try {
        const id = req.params.id;
        const modifiedMovie = new Movie({ ...req.body });
        modifiedMovie._id = id;
        const movieUpdated = await Movie.findByIdAndUpdate(
            id,
            modifiedMovie,
            { new: true }
        );
        return res.status(200).json(movieUpdated);
    } catch (err) {
        next(err);
    }
});

module.exports = moviesRouter;