const express = require('express');
const Cinema = require('../models/Cinemas.js');
const createError = require('../utils/errors/create-error.js');

const cinemasRouter = express.Router();

cinemasRouter.get('/', async (req, res, next) => {
    try {
        const cinemas = await Cinema.find().populate('movies');
        return res.status(200).json(cinemas);
    } catch (err) {
        next(err);
    }
});

cinemasRouter.post('/', async (req, res, next) => {
    try {
        const newCinema = new Cinema({ ...req.body });
        const createdCinema = await newCinema.save();
        return res.status(201).json(createdCinema);
    } catch(err) {
        next(err);
    }
});

cinemasRouter.delete('/:id', async (req, res, next) => {
    try {
        const id = req.params.id;
        await Cinema.findByIdAndDelete(id);
        return res.status(200).json('El cine ha sido eliminado correctamente');
    } catch (err) {
        next(err);
    }
});

//Método para añadir películas a los cines, relacionando ambos modelos. Se podrá ver que películas hay en la cartelera de cada cine.
cinemasRouter.put('/add-movie', async (req, res, next) => {
    try {
        const { cinemaId, movieId } = req.body;
        if(!cinemaId) {
            return next(createError('Se necesita un id de cinema para poder añadir una película', 500));
        }
        if(!movieId) {
            return next(createError('Se necesita un id de pelíclula para añadirla', 500));
        }
        const updatedCinema = await Cinema.findByIdAndUpdate(
            cinemaId,
            { $push: { movies: movieId }},
            { new: true }
        );
        return res.status(200).json(updatedCinema);
    } catch (err) {
        next(err);
    }
});


module.exports = cinemasRouter;


